<!DOCTYPE html>

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<!-- feed -->
	<link href="<?php bloginfo('rss2_url'); ?>" title="RSS - <?php bloginfo('name'); ?>" type="application/rss+xml" rel="alternate">
	<link href="<?php bloginfo('atom_url'); ?>" title="Atom - <?php bloginfo('name'); ?>" type="application/atom+xml" rel="alternate">
	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />
	<!-- the title -->
	<title><?php site_title(); ?></title>
	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class('row'); ?>>
	<div id="container" class="twelve columns">

		<div id="header">
			<h1><?php bloginfo('name'); ?></h1>
			<!-- top nav bar -->
	        <?php $defaults = array(
				'theme_location'  => 'header-menu',
				'menu_class'      => 'nav-bar', 
				'menu_id'         => 'nav',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 2,
				'walker'          => new My_Walker_Nav_Menu()
			); 
			?>
			
			<?php wp_nav_menu( $defaults ); ?>
			
			<!-- masthead image -->
			<div class="masthead">
				<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php bloginfo('name'); ?>" />
			</div><!-- masthead -->
		</div><!-- header -->