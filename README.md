subfoundation
=============

A boilerplate responsive Wordpress theme powered by Foundation, inspired by subtraction.com

Do not, I repeat **do not** install this theme into your site. Subtraction's design is copyrighted. This theme is for educational purposes only.
