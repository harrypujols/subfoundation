<!-- begin sidebar -->
			  <div id="sidebar" class="four columns aside">
			  <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar')) : else : ?>
			  	<h3>Sidebar</h3>
			    <ul class="side-nav">
				    <?php wp_list_pages('title_li='); ?> 
			  	</ul>
				<?php endif; ?>
<!-- optional sidebar widget -->
			  	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar-2')) : else : ?>
			  	<?php endif; ?>
			  	</div><!-- sidebar -->
<!-- end sidebar -->