<?php get_header(); ?>
<!-- main content	 -->	
	<div id="content" class="row">
		<div id="main" class="eight columns">
<!-- begin post -->		
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		    <div <?php post_class('row'); ?>>
			  <div class="three columns aside">
			    <time pubdate="<?php the_time('m-d-Y H:i:s T'); ?>">
				    <?php the_time('F j, Y'); ?>
				</time>
			  </div><!-- aside -->
				<div class="nine columns article">
					<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<?php the_content(); ?>
				</div><!-- article -->
		    </div><!-- post -->
		    <?php endwhile; else: ?>
		    <?php endif; ?>
<!-- end post -->
		</div><!-- main -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>