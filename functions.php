<?php

/* 
TITLE
*/	
	function site_title() {
		wp_title( '|', true, 'right' );
		// add the site's name.
		bloginfo( 'name' );
		// add the site's description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	}
	
/* 
SCRIPTS AND STYLES 
*/

	//add the styles
	function load_styles()
	{
		//register styles for the theme
		wp_register_style( 'screen-style', get_template_directory_uri() . '/style.css', array(), 'all');
		wp_enqueue_style( 'screen-style' );
	}
	add_action( 'wp_enqueue_scripts', 'load_styles' );


	//add the Javascript files
	function load_scripts()
	{
		//register scripts at the footer
		wp_register_script('foundation-mod', get_template_directory_uri() . '/js/foundation/modernizr.foundation.js', array( 'jquery'), ' ', true );
		wp_register_script('foundation-main', get_template_directory_uri() . '/js/foundation/foundation.min.js', ' ', ' ', true );
		wp_register_script('foundation-app', get_template_directory_uri() . '/js/scripts.js', ' ', ' ', true );
		wp_enqueue_script( 'foundation-mod');
		wp_enqueue_script( 'foundation-main');
		wp_enqueue_script( 'foundation-app');
	}
	add_action( 'wp_enqueue_scripts', 'load_scripts', 5 );
	
/* 
TOP NAV BAR 
*/

	//register custom nav bar
	function register_menus() {
		register_nav_menus( array(
			'header-menu' => __( 'Header Menu' ),
		));
	}
	add_action( 'init', 'register_menus' );

	//adds a class to parent menu items
	add_filter('wp_nav_menu_objects', function ($items) {
	$hasSub = function ($menu_item_id, &$items) {
		foreach ($items as $item) {
			if ($item->menu_item_parent && $item->menu_item_parent==$menu_item_id) {
				return true;
				}
			}
			return false;
		};
	
		foreach ($items as &$item) {
			if ($hasSub($item->ID, &$items)) {
				$item->classes[] = 'has-flyout';
			}
		}
		return $items;    
	});
	
	//ads a custom class to the sub-menus
	class My_Walker_Nav_Menu extends Walker_Nav_Menu {
	  function start_lvl(&$output, $depth) {
	    $indent = str_repeat("\t", $depth);
	    $output .= "\n$indent<ul class=\"flyout\">\n";
	  }
	}
	
/* 
HEADER IMAGE
*/
	
	//custom header support
	$defaults = array(
		'default-image'          => 'http://placekitten.com/970/385',
		'random-default'         => false,
		'width'                  => 970,
		'height'                 => 385,
		'flex-height'            => true,
		'flex-width'             => true,
		'header-text'            => false,
		'uploads'                => true
	);
	
	add_theme_support( 'custom-header', $defaults );

/* 
SIDEBAR
*/
	
//register sidebar widgets
  
if ( function_exists('register_sidebar') )
register_sidebar(array(
	'name'          => 'sidebar',
	'before_widget' => '<ul id="%1$s" class="side-nav widget %2$s">',
	'after_widget'  => '</ul>',
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => '</h3>'
));
register_sidebar(array(
	'name'			=> 'sidebar-2',
	'before_widget' => '<ul id="%1$s" class="side-nav widget %2$s">',
	'after_widget'  => '</ul>',
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => '</h3>'
));

?>